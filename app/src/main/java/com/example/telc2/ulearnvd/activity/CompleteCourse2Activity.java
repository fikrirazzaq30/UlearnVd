package com.example.telc2.ulearnvd.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.fragment.CourseForumFragment;
import com.example.telc2.ulearnvd.fragment.CourseHome2Fragment;
import com.example.telc2.ulearnvd.fragment.CourseHomeFragment;
import com.example.telc2.ulearnvd.fragment.CourseKuis2Fragment;
import com.example.telc2.ulearnvd.fragment.CourseKuisFragment;
import com.example.telc2.ulearnvd.fragment.CourseTugasFragment;

import java.util.ArrayList;
import java.util.List;

public class CompleteCourse2Activity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_course2);
        setTitle("Ekonomi Kelas 12");

        toolbar = (Toolbar) findViewById(R.id.toolbar_complete_course);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                toolbar.setContentDescription("Kembali");
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewpager_complete_course);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs_complete_course);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        tabLayout.setContentDescription("Course Home");
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        tabLayout.setContentDescription("Kuis");
                        break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        tabLayout.setContentDescription("Tugas");
                        break;
                    case 3:
                        viewPager.setCurrentItem(3);
                        tabLayout.setContentDescription("Forum");
                        break;
                    default:
                        viewPager.setCurrentItem(tab.getPosition());
                        toolbar.setTitle("Ulearning");
                        tabLayout.setContentDescription(toolbar.getTitle().toString());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CourseHome2Fragment(), "Course Home");
        adapter.addFragment(new CourseKuis2Fragment(), "Kuis");
        adapter.addFragment(new CourseTugasFragment(), "Tugas");
        adapter.addFragment(new CourseForumFragment(), "Forum");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}