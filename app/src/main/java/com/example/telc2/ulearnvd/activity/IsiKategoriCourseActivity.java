package com.example.telc2.ulearnvd.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.adapter.IsiKategoriCourseAdapter;
import com.example.telc2.ulearnvd.adapter.KategoriCourseAdapter;
import com.example.telc2.ulearnvd.adapter.LessonBabAdapter;
import com.example.telc2.ulearnvd.data.model.IsiKategoriCourse;
import com.example.telc2.ulearnvd.data.model.KategoriCourse;
import com.example.telc2.ulearnvd.data.model.LessonBab;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class IsiKategoriCourseActivity extends AppCompatActivity implements ItemClickListener {

    private List<IsiKategoriCourse> isiKategoriCourseList = new ArrayList<>();
    private IsiKategoriCourseAdapter isiKategoriCourseAdapter;
    private RecyclerView rcvIsiKategoriCourse;
    private TextView txCourseName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_kategori_course);
        setTitle("Biologi");

        //--Pair View
        rcvIsiKategoriCourse = (RecyclerView) findViewById(R.id.rcv_isi_kategori_course);
        txCourseName = (TextView) findViewById(R.id.tx_isi_kategori_course_nama_course);

        //--Adapter Riwayat Pembelajaran
        isiKategoriCourseAdapter = new IsiKategoriCourseAdapter(isiKategoriCourseList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(this);
        rcvIsiKategoriCourse.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rcvIsiKategoriCourse.setLayoutManager(mLayoutManagerCourse);
        rcvIsiKategoriCourse.setItemAnimator(new DefaultItemAnimator());
        rcvIsiKategoriCourse.setAdapter(isiKategoriCourseAdapter);
        isiKategoriCourseAdapter.setClickListener(this);

//        rcvIsiKategoriCourse.setContentDescription(txCourseName.getText().toString());

        //--Back Button Action Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //--Populate Data Lesson Bab
        isiKategoriCourseList.clear();
        dummyIsiKategoriCourseData();
    }

    public void dummyIsiKategoriCourseData() {
        IsiKategoriCourse isiKategoriCourse;

        isiKategoriCourse = new IsiKategoriCourse("Biologi Kelas 12" , "http://juvetic.telclab.com/uelarn/images/Ipa%20besar.png");
        isiKategoriCourseList.add(isiKategoriCourse);


        isiKategoriCourseAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view, int position) {
        final IsiKategoriCourse isiKategoriCourse = isiKategoriCourseList.get(position);
        Intent intent = new Intent(IsiKategoriCourseActivity.this, DetilCourseActivity.class);
        startActivity(intent);
    }
}