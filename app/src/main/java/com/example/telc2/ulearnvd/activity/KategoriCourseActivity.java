package com.example.telc2.ulearnvd.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.adapter.IsiKategoriCourseAdapter;
import com.example.telc2.ulearnvd.adapter.KategoriCourseAdapter;
import com.example.telc2.ulearnvd.adapter.LessonBabAdapter;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.data.model.IsiKategoriCourse;
import com.example.telc2.ulearnvd.data.model.KategoriCourse;
import com.example.telc2.ulearnvd.data.model.LessonBab;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class KategoriCourseActivity extends AppCompatActivity implements ItemClickListener {

    private List<KategoriCourse> kategoriCourseList = new ArrayList<>();
    private KategoriCourseAdapter kategoriCourseAdapter;
    private RecyclerView rcvKategoriCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategori_course);
        setTitle("Kategori Pelajaran");

        //--Pair View
        rcvKategoriCourse = (RecyclerView) findViewById(R.id.rcv_kategori_course);

        //--Adapter Riwayat Pembelajaran
        kategoriCourseAdapter = new KategoriCourseAdapter(kategoriCourseList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(this);
        rcvKategoriCourse.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rcvKategoriCourse.setLayoutManager(mLayoutManagerCourse);
        rcvKategoriCourse.setItemAnimator(new DefaultItemAnimator());
        rcvKategoriCourse.setAdapter(kategoriCourseAdapter);
        kategoriCourseAdapter.setClickListener(this);

        //--Back Button Action Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeActionContentDescription("Balik ke halaman beranda");

        //--Populate Data Lesson Bab
        kategoriCourseList.clear();
        dummyKategoriCourseData();
    }

    public void dummyKategoriCourseData() {
        KategoriCourse kategoriCourse;

        for (int i = 1; i <= 3; i++) {
            kategoriCourse = new KategoriCourse("Biologi");
            kategoriCourseList.add(kategoriCourse);
        }

        kategoriCourseAdapter.notifyDataSetChanged();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view, int position) {
        final KategoriCourse kategoriCourse = kategoriCourseList.get(position);
        Intent intent = new Intent(this, IsiKategoriCourseActivity.class);
        startActivity(intent);
    }
}