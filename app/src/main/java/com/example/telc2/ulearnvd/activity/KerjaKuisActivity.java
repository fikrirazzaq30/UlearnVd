package com.example.telc2.ulearnvd.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.ulearnvd.R;

import org.w3c.dom.Text;

import java.io.IOException;

public class KerjaKuisActivity extends AppCompatActivity {

    private TextView nomorKuis;
    private CardView cvDeskKuis;

    private Button btnMulaiKuis;
    private TextView txWaktuKuis;
    private TextView txPertanyaan;
    private TextView txNomorPertanyaan;
    private TextView txSoal;
    private RadioGroup radioGroupPg;

    private ImageButton btnRekam;
    private EditText edtIsian;

    private Button btnSimpan;

    private Button btnSebelumnya;
    private TextView txHalaman;
    private Button btnSelanjutnya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kerja_kuis);
        setTitle("Assesment Pertumbuhan, Perkembangan dan Pemersiban");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nomorKuis = (TextView) findViewById(R.id.tx_item_kuis_course_kuis_nomor);
        cvDeskKuis = (CardView) findViewById(R.id.cv_item_course_kuis);

        btnMulaiKuis = (Button) findViewById(R.id.btn_mulai_kuis);

        txWaktuKuis = (TextView) findViewById(R.id.tx_kuis_waktu);

//        txPertanyaan = (TextView) findViewById(R.id.tx_kuis_pertanyaan);
//        txNomorPertanyaan = (TextView) findViewById(R.id.tx_kuis_nomor_pertanyaan_nomor);

        txSoal = (TextView) findViewById(R.id.tx_kuis_soal);

//        radioGroupPg = (RadioGroup) findViewById(R.id.radioGrupPG);
//
//        btnRekam = (ImageButton) findViewById(R.id.btn_kuis_rekam);
//        edtIsian = (EditText) findViewById(R.id.edt_kuis_isian);

        btnSimpan = (Button) findViewById(R.id.btn_kuis_simpan);

        btnSebelumnya = (Button) findViewById(R.id.btn_kuis_sebelumnya);
        btnSelanjutnya = (Button) findViewById(R.id.btn_kuis_selanjutnya);
        txHalaman = (TextView) findViewById(R.id.tx_kuis_halaman);

        txWaktuKuis.setVisibility(View.GONE);
        txPertanyaan.setVisibility(View.GONE);
        txNomorPertanyaan.setVisibility(View.GONE);
        txSoal.setVisibility(View.GONE);
        radioGroupPg.setVisibility(View.GONE);
        btnRekam.setVisibility(View.GONE);
        edtIsian.setVisibility(View.GONE);
        btnSimpan.setVisibility(View.GONE);
        btnSebelumnya.setVisibility(View.INVISIBLE);
        btnSelanjutnya.setVisibility(View.INVISIBLE);
        txHalaman.setVisibility(View.INVISIBLE);

        btnMulaiKuis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomorKuis.setVisibility(View.GONE);
                cvDeskKuis.setVisibility(View.GONE);

                txWaktuKuis.setVisibility(View.VISIBLE);
                txPertanyaan.setVisibility(View.VISIBLE);
                txNomorPertanyaan.setVisibility(View.VISIBLE);
                txNomorPertanyaan.setText("1");
                txSoal.setVisibility(View.VISIBLE);
                txSoal.setText("Dengan apa tumbuhan bernafas?");
                radioGroupPg.setVisibility(View.VISIBLE);

                btnRekam.setVisibility(View.GONE);
                edtIsian.setVisibility(View.GONE);

                btnSimpan.setVisibility(View.VISIBLE);
                btnSelanjutnya.setVisibility(View.VISIBLE);
                txHalaman.setVisibility(View.VISIBLE);
                txHalaman.setText("1/2");
            }
        });

        btnSebelumnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomorKuis.setVisibility(View.GONE);
                cvDeskKuis.setVisibility(View.GONE);

                txWaktuKuis.setVisibility(View.VISIBLE);
                txPertanyaan.setVisibility(View.VISIBLE);
                txNomorPertanyaan.setVisibility(View.VISIBLE);
                txNomorPertanyaan.setText("1");
                txSoal.setVisibility(View.VISIBLE);
                txSoal.setText("Dengan apa tumbuhan bernafas?");
                radioGroupPg.setVisibility(View.VISIBLE);

                btnRekam.setVisibility(View.GONE);
                edtIsian.setVisibility(View.GONE);

                btnSimpan.setVisibility(View.VISIBLE);
                btnSelanjutnya.setVisibility(View.VISIBLE);
                btnSebelumnya.setVisibility(View.INVISIBLE);
                txHalaman.setVisibility(View.VISIBLE);
                txHalaman.setText("1/2");
            }
        });

        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomorKuis.setVisibility(View.GONE);
                cvDeskKuis.setVisibility(View.GONE);

                txWaktuKuis.setVisibility(View.VISIBLE);
                txWaktuKuis.setText("Waktu 9 Menit 30 Detik");
                txPertanyaan.setVisibility(View.VISIBLE);
                txNomorPertanyaan.setVisibility(View.VISIBLE);
                txNomorPertanyaan.setText("2");
                txSoal.setVisibility(View.VISIBLE);
                txSoal.setText("Jelaskan perbedaan antara pertumbuhan primer dan pertumbuhan sekunder!");
                radioGroupPg.setVisibility(View.GONE);

                btnRekam.setVisibility(View.VISIBLE);
                edtIsian.setVisibility(View.VISIBLE);

                btnSimpan.setVisibility(View.VISIBLE);
                btnSebelumnya.setVisibility(View.VISIBLE);
                btnSelanjutnya.setVisibility(View.INVISIBLE);
                txHalaman.setVisibility(View.VISIBLE);
                txHalaman.setText("2/2");
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int choice) {
                        switch (choice) {
                            case DialogInterface.BUTTON_POSITIVE:
                                finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(KerjaKuisActivity.this);
                builder.setMessage("Anda yakin?")
                        .setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int choice) {
                        switch (choice) {
                            case DialogInterface.BUTTON_POSITIVE:
                                onBackPressed();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Keluar dari kuis?")
                        .setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}