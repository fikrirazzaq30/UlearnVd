package com.example.telc2.ulearnvd.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.telc2.ulearnvd.R;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Locale;

import static com.miguelcatalan.materialsearchview.MaterialSearchView.REQUEST_VOICE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtEmail, edtPassword;
    private ImageButton imgbtnUsername, imgbtnPassword;
    private Button btnLogin, btnRegistrasi, btnLupaPassword, btnHelp;
    public static final int REQUEST_VOICE_USERNAME = 9998;
    public static final int REQUEST_VOICE_PASSWORD = 9997;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //--Pair View
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegistrasi = (Button) findViewById(R.id.btn_registrasi);
        btnLupaPassword = (Button) findViewById(R.id.btn_lupa_password);
        btnHelp = (Button) findViewById(R.id.btn_help);
        imgbtnUsername = (ImageButton) findViewById(R.id.btn_voice_username);
        imgbtnPassword = (ImageButton) findViewById(R.id.btn_voice_password);

        imgbtnUsername.setContentDescription("Masukan nama pengguna menggunakan suara");
        imgbtnPassword.setContentDescription("Masukan kata sandi menggunakan suara");

        //--Enable Click Handling
        btnLogin.setOnClickListener(this);
        btnRegistrasi.setOnClickListener(this);
        btnLupaPassword.setOnClickListener(this);
        btnHelp.setOnClickListener(this);
        edtEmail.setOnClickListener(this);
        edtPassword.setOnClickListener(this);
        imgbtnUsername.setOnClickListener(this);
        imgbtnPassword.setOnClickListener(this);

        //--Labelling UI
//       edtEmail.setContentDescription("Masukan email");
//       edtPassword.setContentDescription("masukan password");
    }


    @Override
    public void onClick(View v) {
        if (v == btnLogin) {
            btnLogin.setContentDescription(getString(R.string.btn_login_masuk));
            if (!TextUtils.isEmpty(edtEmail.getText().toString())) {
                if (!TextUtils.isEmpty(edtPassword.getText().toString())) {
                    if (edtEmail.getText().toString().equals("admin")) {
                        if (edtPassword.getText().toString().equals("admin")) {
                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            intent.putExtra("namalengkap", "Admin");
                            startActivity(intent);
                        }
                        else {
                            final Dialog dialog = new Dialog(this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_password_salah);
                            dialog.show();
                            edtPassword.setContentDescription(getString(R.string.tx_login_password_salah));
                        }
                    }
                    else if (edtEmail.getText().toString().equals("agum")) {
                        if (edtPassword.getText().toString().equals("kresna123")) {
                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            intent.putExtra("namalengkap", "Agum Junianto");
                            startActivity(intent);
                        } else {
                            final Dialog dialog = new Dialog(this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_password_salah);
                            dialog.show();
                            edtPassword.setContentDescription(getString(R.string.tx_login_password_salah));
                        }
                    }
                    else if (edtEmail.getText().toString().equals("veronikha")) {
                        if (edtPassword.getText().toString().equals("veronikha")) {
                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            intent.putExtra("namalengkap", "Veronikha Evendy");
                            startActivity(intent);
                        } else {
                            final Dialog dialog = new Dialog(this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_password_salah);
                            dialog.show();
                            edtPassword.setContentDescription(getString(R.string.tx_login_password_salah));
                        }
                    }
                    else if (edtEmail.getText().toString().equals("kresna")) {
                        if (edtPassword.getText().toString().equals("kresna123")) {
                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            intent.putExtra("namalengkap", "Muh Kresna dwi wahana");
                            startActivity(intent);
                        } else {
                            final Dialog dialog = new Dialog(this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_password_salah);
                            dialog.show();
                            edtPassword.setContentDescription(getString(R.string.tx_login_password_salah));
                        }
                    }
                    else if (edtEmail.getText().toString().equals("Jajang")) {
                        if (edtPassword.getText().toString().equals("jajang123")) {
                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            intent.putExtra("namalengkap", "Jajang Abdurohman");
                            startActivity(intent);
                        } else {
                            final Dialog dialog = new Dialog(this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_password_salah);
                            dialog.show();
                            edtPassword.setContentDescription(getString(R.string.tx_login_password_salah));
                        }
                    }


                    else {
                        final Dialog dialog = new Dialog(this) {
                            @Override
                            public boolean onTouchEvent(MotionEvent event) {
                                this.dismiss();
                                return true;
                            }
                        };

                        dialog.setContentView(R.layout.alert_username_salah);
                        dialog.show();
                        edtEmail.setContentDescription(getString(R.string.tx_login_email_salah));
                    }

                }
                else {
                    final Dialog dialog = new Dialog(this) {
                        @Override
                        public boolean onTouchEvent(MotionEvent event) {
                            this.dismiss();
                            return true;
                        }
                    };

                    dialog.setContentView(R.layout.alert_password_kosong);
                    dialog.show();
                    edtPassword.setContentDescription(getString(R.string.tx_login_password_tidak_kosong));
                }
            }
            else {
                final Dialog dialog = new Dialog(this) {
                    @Override
                    public boolean onTouchEvent(MotionEvent event) {
                        this.dismiss();
                        return true;
                    }
                };

                dialog.setContentView(R.layout.alert_username_kosong);
                dialog.show();
                edtEmail.setContentDescription(getString(R.string.tx_login_email_tidak_kosong));
            }

        }
        else if (v == btnRegistrasi) {
            btnRegistrasi.setContentDescription("Tombol Registrasi");
            Intent intent = new Intent(LoginActivity.this, RegistrasiActivity.class);
            startActivity(intent);
        }
        else if (v == btnLupaPassword) {
            btnLupaPassword.setContentDescription(btnLupaPassword.getText().toString());
        }
        else if (v == btnHelp) {
            btnHelp.setContentDescription(btnHelp.getText().toString());
        }
        else if (v == imgbtnUsername) {
            promptSpeechInput(REQUEST_VOICE_USERNAME);
        }
        else if (v == imgbtnPassword) {
            promptSpeechInput(REQUEST_VOICE_PASSWORD);
        }
    }

    private static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity)cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper)cont).getBaseContext());
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VOICE_USERNAME && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtEmail.setText(searchWrd);

                }
            }

            return;
        }
        else if (requestCode == REQUEST_VOICE_PASSWORD && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtPassword.setText(searchWrd);

                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void promptSpeechInput(int req) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        try {
            startActivityForResult(intent, req);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(), "Tidak dapat merekam suara",
                    Toast.LENGTH_SHORT).show();
        }
    }
}