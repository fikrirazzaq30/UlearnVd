package com.example.telc2.ulearnvd.activity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.fragment.HomeFragment;

import java.util.ArrayList;
import java.util.Locale;

public class RegistrasiActivity extends AppCompatActivity {

    //Biodata
    private RelativeLayout rlRegisKetBiodata, rlRegisFormBiodata;
    private EditText edtNamaLengkap, edtUsername, edtEmail, edtPassword, edtConfirmPassword;

    //Sekolah
    private RelativeLayout rlRegisKetSekolah, rlRegisFormSekolah;
    private LinearLayout llSma, llKuliah, llUmum;
    private EditText edtSma, edtKuliah, edtUmum;

    //Lokasi
    private RelativeLayout rlRegisKetLokasi, rlRegisFormLokasi;
    private EditText edtLokasi, edtInstitusi, edtJurusan;

    private Button btnNext, btnFinish;

    private ImageButton btnNamaLengkap, btnUsername, btnEmail, btnPassword, btnKonfPassword, btnLokasi, btnInstitusi;

    public static final int REQUEST_VOICE_NAMA = 9999;
    public static final int REQUEST_VOICE_USERNAME = 9998;
    public static final int REQUEST_VOICE_EMAIL = 9997;
    public static final int REQUEST_VOICE_PASSWORD = 9996;
    public static final int REQUEST_VOICE_KONFPASSWORD = 9995;
    public static final int REQUEST_VOICE_LOKASI = 9994;
    public static final int REQUEST_VOICE_INSTITUSI = 9993;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        setTitle("Registrasi");

        btnLokasi = (ImageButton) findViewById(R.id.btn_regis_voice_lokasi);
        btnInstitusi = (ImageButton) findViewById(R.id.btn_regis_voice_institusi);

        rlRegisKetBiodata = (RelativeLayout) findViewById(R.id.rl_regis_ket_biodata);
        rlRegisFormBiodata = (RelativeLayout) findViewById(R.id.rl_regis_form_biodata);
        edtNamaLengkap = (EditText) findViewById(R.id.edt_regis_namalengkap);
        edtUsername = (EditText) findViewById(R.id.edt_regis_username);
        edtEmail = (EditText) findViewById(R.id.edt_regis_email);
        edtPassword = (EditText) findViewById(R.id.edt_regis_password);
        edtConfirmPassword = (EditText) findViewById(R.id.edt_regis_konfpassword);

        edtUsername.setContentDescription("Masukkan nama pengguna");
        edtEmail.setContentDescription("Masukkan email");
        edtPassword.setContentDescription("Masukkan kata sandi");
        edtConfirmPassword.setContentDescription("Ulangi kata sandi");

        rlRegisKetSekolah = (RelativeLayout) findViewById(R.id.rl_regis_ket_sekolah);
        rlRegisFormSekolah = (RelativeLayout) findViewById(R.id.rl_regis_form_sekolah);
        llSma = (LinearLayout) findViewById(R.id.ll_regis_sma);
        llKuliah = (LinearLayout) findViewById(R.id.ll_regis_kuliah);
        llUmum = (LinearLayout) findViewById(R.id.ll_regis_umum);
        edtSma = (EditText) findViewById(R.id.edt_regis_sma);
        edtKuliah = (EditText) findViewById(R.id.edt_regis_kuliah);
        edtUmum = (EditText) findViewById(R.id.edt_regis_umum);

        edtSma.setContentDescription("Pilih Tingkat SMA");
        edtKuliah.setContentDescription("Pilih Tingkat Kuliah");
        edtUmum.setContentDescription("Pilih Umum");

        btnNamaLengkap = (ImageButton) findViewById(R.id.btn_regis_voice_namalengkap);
        btnUsername = (ImageButton) findViewById(R.id.btn_regis_voice_username);
        btnEmail = (ImageButton) findViewById(R.id.btn_regis_voice_email);
        btnPassword = (ImageButton) findViewById(R.id.btn_regis_voice_password);
        btnKonfPassword = (ImageButton) findViewById(R.id.btn_regis_voice_konfpassword);

        btnNamaLengkap.setContentDescription("Masukkan nama lengkap menggunakan suara");
        btnUsername.setContentDescription("Masukkan nama lengkap menggunakan suara");
        btnEmail.setContentDescription("Masukkan nama lengkap menggunakan suara");
        btnPassword.setContentDescription("Masukkan nama lengkap menggunakan suara");
        btnKonfPassword.setContentDescription("Masukkan nama lengkap menggunakan suara");

        rlRegisKetLokasi = (RelativeLayout) findViewById(R.id.rl_regis_ket_loksekolah);
        rlRegisFormLokasi = (RelativeLayout) findViewById(R.id.rl_regis_loksekolah);
        edtLokasi = (EditText) findViewById(R.id.edt_regis_loksekolah_lokasi);
        edtInstitusi = (EditText) findViewById(R.id.edt_regis_loksekolah_institusi);

        edtLokasi.setContentDescription("Masukkan nama lokasi");
        edtInstitusi.setContentDescription("Masukkan nama institusi");

        btnNext = (Button) findViewById(R.id.btn_regis_next);
        btnFinish = (Button) findViewById(R.id.btn_regis_finish);
        btnFinish.setVisibility(View.GONE);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtNamaLengkap.getText().toString())) {
                    if (!TextUtils.isEmpty(edtUsername.getText().toString())) {
                        if (!TextUtils.isEmpty(edtEmail.getText().toString())) {
                            if (!TextUtils.isEmpty(edtPassword.getText().toString())) {
                                if (!TextUtils.isEmpty(edtConfirmPassword.getText().toString())) {
                                    if (edtConfirmPassword.getText().toString().equals(edtPassword.getText().toString())) {
                                        rlRegisKetBiodata.setVisibility(View.GONE);
                                        rlRegisFormBiodata.setVisibility(View.GONE);
                                        edtNamaLengkap.setVisibility(View.GONE);
                                        edtUsername.setVisibility(View.GONE);
                                        edtEmail.setVisibility(View.GONE);
                                        edtPassword.setVisibility(View.GONE);
                                        edtConfirmPassword.setVisibility(View.GONE);

                                        rlRegisKetSekolah.setVisibility(View.VISIBLE);
                                        rlRegisFormSekolah.setVisibility(View.VISIBLE);
                                        llSma.setVisibility(View.VISIBLE);
                                        llKuliah.setVisibility(View.VISIBLE);
                                        llUmum.setVisibility(View.VISIBLE);

                                        rlRegisKetLokasi.setVisibility(View.GONE);
                                        rlRegisFormLokasi.setVisibility(View.GONE);
                                        edtLokasi.setVisibility(View.GONE);
                                        edtInstitusi.setVisibility(View.GONE);

                                        btnNext.setVisibility(View.GONE);
                                    }
                                    else {
                                        final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                                            @Override
                                            public boolean onTouchEvent(MotionEvent event) {
                                                this.dismiss();
                                                return true;
                                            }
                                        };

                                        dialog.setContentView(R.layout.alert_password_tidaksama);
                                        dialog.show();
                                    }
                                }
                                else {
                                    final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                                        @Override
                                        public boolean onTouchEvent(MotionEvent event) {
                                            this.dismiss();
                                            return true;
                                        }
                                    };

                                    dialog.setContentView(R.layout.alert_kosong);
                                    dialog.show();
                                }
                            }
                            else {
                                final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                                    @Override
                                    public boolean onTouchEvent(MotionEvent event) {
                                        this.dismiss();
                                        return true;
                                    }
                                };

                                dialog.setContentView(R.layout.alert_kosong);
                                dialog.show();
                            }
                        }
                        else {
                            final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_kosong);
                            dialog.show();
                        }
                    }
                    else {
                        final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                            @Override
                            public boolean onTouchEvent(MotionEvent event) {
                                this.dismiss();
                                return true;
                            }
                        };

                        dialog.setContentView(R.layout.alert_kosong);
                        dialog.show();
                    }
                }
                else {
                    final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                        @Override
                        public boolean onTouchEvent(MotionEvent event) {
                            this.dismiss();
                            return true;
                        }
                    };

                    dialog.setContentView(R.layout.alert_kosong);
                    dialog.show();
                }
            }
        });

        edtSma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlRegisKetBiodata.setVisibility(View.GONE);
                rlRegisFormBiodata.setVisibility(View.GONE);
                edtNamaLengkap.setVisibility(View.GONE);
                edtUsername.setVisibility(View.GONE);
                edtEmail.setVisibility(View.GONE);
                edtPassword.setVisibility(View.GONE);
                edtConfirmPassword.setVisibility(View.GONE);

                rlRegisKetSekolah.setVisibility(View.GONE);
                rlRegisFormSekolah.setVisibility(View.GONE);
                llSma.setVisibility(View.GONE);
                llKuliah.setVisibility(View.GONE);
                llUmum.setVisibility(View.GONE);

                rlRegisKetLokasi.setVisibility(View.VISIBLE);
                rlRegisFormLokasi.setVisibility(View.VISIBLE);
                edtLokasi.setVisibility(View.VISIBLE);
                edtInstitusi.setVisibility(View.VISIBLE);

                btnNext.setVisibility(View.GONE);
                btnFinish.setVisibility(View.VISIBLE);
            }
        });

        edtKuliah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlRegisKetBiodata.setVisibility(View.GONE);
                rlRegisFormBiodata.setVisibility(View.GONE);
                edtNamaLengkap.setVisibility(View.GONE);
                edtUsername.setVisibility(View.GONE);
                edtEmail.setVisibility(View.GONE);
                edtPassword.setVisibility(View.GONE);
                edtConfirmPassword.setVisibility(View.GONE);

                rlRegisKetSekolah.setVisibility(View.GONE);
                rlRegisFormSekolah.setVisibility(View.GONE);
                llSma.setVisibility(View.GONE);
                llKuliah.setVisibility(View.GONE);
                llUmum.setVisibility(View.GONE);

                rlRegisKetLokasi.setVisibility(View.VISIBLE);
                rlRegisFormLokasi.setVisibility(View.VISIBLE);
                edtLokasi.setVisibility(View.VISIBLE);
                edtInstitusi.setVisibility(View.VISIBLE);

                btnNext.setVisibility(View.GONE);
                btnFinish.setVisibility(View.VISIBLE);
            }
        });

        edtUmum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlRegisKetBiodata.setVisibility(View.GONE);
                rlRegisFormBiodata.setVisibility(View.GONE);
                edtNamaLengkap.setVisibility(View.GONE);
                edtUsername.setVisibility(View.GONE);
                edtEmail.setVisibility(View.GONE);
                edtPassword.setVisibility(View.GONE);
                edtConfirmPassword.setVisibility(View.GONE);

                rlRegisKetSekolah.setVisibility(View.GONE);
                rlRegisFormSekolah.setVisibility(View.GONE);
                llSma.setVisibility(View.GONE);
                llKuliah.setVisibility(View.GONE);
                llUmum.setVisibility(View.GONE);

                rlRegisKetLokasi.setVisibility(View.VISIBLE);
                rlRegisFormLokasi.setVisibility(View.VISIBLE);
                edtLokasi.setVisibility(View.VISIBLE);
                edtInstitusi.setVisibility(View.VISIBLE);

                btnNext.setVisibility(View.GONE);
                btnFinish.setVisibility(View.VISIBLE);
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtLokasi.getText().toString())) {
                    if (!TextUtils.isEmpty(edtInstitusi.getText().toString())) {
                        if (!TextUtils.isEmpty(edtJurusan.getText().toString())) {
                            Intent intent = new Intent(RegistrasiActivity.this, DashboardActivity.class);
                            intent.putExtra("namalengkap", edtNamaLengkap.getText().toString());
                            startActivity(intent);
                        }
                        else {
                            final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                                @Override
                                public boolean onTouchEvent(MotionEvent event) {
                                    this.dismiss();
                                    return true;
                                }
                            };

                            dialog.setContentView(R.layout.alert_kosong);
                            dialog.show();
                        }
                    }
                    else {
                        final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                            @Override
                            public boolean onTouchEvent(MotionEvent event) {
                                this.dismiss();
                                return true;
                            }
                        };

                        dialog.setContentView(R.layout.alert_kosong);
                        dialog.show();
                    }
                }
                else {
                    final Dialog dialog = new Dialog(RegistrasiActivity.this) {
                        @Override
                        public boolean onTouchEvent(MotionEvent event) {
                            this.dismiss();
                            return true;
                        }
                    };

                    dialog.setContentView(R.layout.alert_kosong);
                    dialog.show();
                }
            }
        });

        btnNamaLengkap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_NAMA);
            }
        });

        btnUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_USERNAME);
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_EMAIL);
            }
        });

        btnPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_PASSWORD);
            }
        });

        btnKonfPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_KONFPASSWORD);
            }
        });

        btnInstitusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_INSTITUSI);
            }
        });

        btnLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(REQUEST_VOICE_LOKASI);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VOICE_NAMA && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtNamaLengkap.setText(searchWrd);
                }
            }

            return;
        }
        else if (requestCode == REQUEST_VOICE_USERNAME && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtUsername.setText(searchWrd);
                }
            }

            return;
        }
        else if (requestCode == REQUEST_VOICE_EMAIL && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtEmail.setText(searchWrd);
                }
            }

            return;
        }
        else if (requestCode == REQUEST_VOICE_PASSWORD && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtPassword.setText(searchWrd);
                }
            }

            return;
        }
        else if (requestCode == REQUEST_VOICE_KONFPASSWORD && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    edtConfirmPassword.setText(searchWrd);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void promptSpeechInput(int req) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        try {
            startActivityForResult(intent, req);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(), "Tidak dapat merekam suara",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
