package com.example.telc2.ulearnvd.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.data.model.CourseKuis;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.List;

import static com.example.telc2.ulearnvd.R.id.tx_item_course_kuis_judul;
import static com.example.telc2.ulearnvd.R.id.tx_item_course_kuis_pertanyaan;

/**
 * Created by TEL-C on 8/8/17.
 */

public class CourseKuisAdapter extends RecyclerView.Adapter<CourseKuisAdapter.HolderData> {

    private List<CourseKuis> courseKuisList;
    private ItemClickListener clickListener;

    public CourseKuisAdapter(List<CourseKuis> courseKuisList) {
        this.courseKuisList = courseKuisList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragment_course_kuis, parent, false);
        return new CourseKuisAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        CourseKuis courseKuis = courseKuisList.get(position);
        holder.txNomorKuis.setText(courseKuis.getNomorKuis());
        holder.txJudulLessonBab.setText(courseKuis.getJudulLessonBab());
        holder.txJmlPertanyaan.setText(courseKuis.getJmlPertanyaan());
        holder.txWaktu.setText(courseKuis.getWaktu());
        holder.txSkor.setText(courseKuis.getSkor());
        holder.txLulus.setText(courseKuis.getLulus());
    }

    @Override
    public int getItemCount() {
        return courseKuisList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txNomorKuis, txJudulLessonBab, txJmlPertanyaan, txWaktu, txSkor, txLulus;

        public HolderData(View itemView) {
            super(itemView);

            txNomorKuis = (TextView) itemView.findViewById(R.id.tx_item_course_kuis_nomor);
            txJudulLessonBab = (TextView) itemView.findViewById(tx_item_course_kuis_judul);
            txJmlPertanyaan = (TextView) itemView.findViewById(tx_item_course_kuis_pertanyaan);
            txWaktu = (TextView) itemView.findViewById(R.id.tx_item_course_kuis_waktu);
            txSkor = (TextView) itemView.findViewById(R.id.tx_item_course_kuis_score);
            txLulus = (TextView) itemView.findViewById(R.id.tx_item_course_kuis_lulus);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}
