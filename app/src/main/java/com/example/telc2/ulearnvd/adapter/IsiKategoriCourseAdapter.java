package com.example.telc2.ulearnvd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.IsiKategoriCourse;
import com.example.telc2.ulearnvd.helper.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TEL-C on 8/9/17.
 */

public class IsiKategoriCourseAdapter extends RecyclerView.Adapter<IsiKategoriCourseAdapter.HolderData> {

    private List<IsiKategoriCourse> isiKategoriCourseList;
    private ItemClickListener clickListener;
    private Context mContext;

    public IsiKategoriCourseAdapter(List<IsiKategoriCourse> isiKategoriCourseList, Context context) {
        this.isiKategoriCourseList = isiKategoriCourseList;
        mContext = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity_isi_kategori_course, parent, false);
        return new IsiKategoriCourseAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        IsiKategoriCourse isiKategoriCourse = isiKategoriCourseList.get(position);
        holder.txNamaCourse.setText(isiKategoriCourse.getNamaIsiKategoriCourse());
        Picasso.with(mContext).load(isiKategoriCourseList.get(position).getGambarIsiKategoriCourse()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return isiKategoriCourseList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txNamaCourse;
        private ImageView imageView;

        public HolderData(View itemView) {
            super(itemView);

            txNamaCourse = (TextView) itemView.findViewById(R.id.tx_isi_kategori_course_nama_course);
            imageView = (ImageView) itemView.findViewById(R.id.img_isi_kategori_course_gambar);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}
