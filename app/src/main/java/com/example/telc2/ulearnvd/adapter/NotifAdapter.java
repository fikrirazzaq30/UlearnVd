package com.example.telc2.ulearnvd.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.Notif;

import java.util.List;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.HolderData> {

    private List<Notif> notifList;

    public NotifAdapter(List<Notif> notifList) {
        this.notifList = notifList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragment_notif, parent, false);
        return new HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        Notif notif = notifList.get(position);
        holder.txNamaProfil.setText(notif.getNamaProfil());
        holder.txNamaCourse.setText(notif.getNamaCourse());
        holder.txWaktu.setText(notif.getWaktu());
    }

    @Override
    public int getItemCount() {
        return notifList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {

        public TextView txNamaProfil, txNamaCourse, txWaktu;

        public HolderData(View itemView) {
            super(itemView);
            txNamaProfil = (TextView) itemView.findViewById(R.id.tx_notif_nama);
            txNamaCourse = (TextView) itemView.findViewById(R.id.tx_notif_coursename);
            txWaktu = (TextView) itemView.findViewById(R.id.tx_notif_waktu);
        }
    }
}
