package com.example.telc2.ulearnvd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.RiwayatPembelajaran;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.List;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class RiwayatPembelajaranAdapter extends RecyclerView.Adapter<RiwayatPembelajaranAdapter.HolderData> {

    private List<RiwayatPembelajaran> riwayatPembelajaranList;

    private ItemClickListener clickListener;
    private Context mContext;

    public RiwayatPembelajaranAdapter(List<RiwayatPembelajaran> riwayatPembelajaranList, Context context) {
        this.riwayatPembelajaranList = riwayatPembelajaranList;
        mContext = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragment_home_riwayat_pembelajaran, parent, false);
        return new RiwayatPembelajaranAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        RiwayatPembelajaran riwayatPembelajaran = riwayatPembelajaranList.get(position);
        holder.txNomorLesson.setText(riwayatPembelajaran.getNomerLesson());
        holder.txNamaLesson.setText(riwayatPembelajaran.getNamaLesson());
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return riwayatPembelajaranList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txNomorLesson, txNamaLesson;

        public HolderData(View itemView) {
            super(itemView);
            txNomorLesson = (TextView) itemView.findViewById(R.id.tx_home_nomor_lesson);
            txNamaLesson = (TextView) itemView.findViewById(R.id.tx_home_nama_lesson);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}