package com.example.telc2.ulearnvd.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.RiwayatUjian;
import com.example.telc2.ulearnvd.data.model.SubLesson;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.List;

/**
 * Created by TEL-C on 8/11/17.
 */

public class SubLessonAdapter extends RecyclerView.Adapter<SubLessonAdapter.HolderData> {

    private List<SubLesson> subLessonList;
    ItemClickListener clickListener;

    public SubLessonAdapter(List<SubLesson> subLessonList) {
        this.subLessonList = subLessonList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity_isi_lesson, parent, false);
        return new SubLessonAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        SubLesson subLesson = subLessonList.get(position);
        holder.txIsiLesson.setText(subLesson.getNama());
    }

    @Override
    public int getItemCount() {
        return subLessonList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgIsiLesson;
        TextView txIsiLesson;

        public HolderData(View itemView) {
            super(itemView);

            imgIsiLesson = (ImageView) itemView.findViewById(R.id.img_item_isi_lesson);
            txIsiLesson = (TextView) itemView.findViewById(R.id.tx_item_isi_lesson);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}