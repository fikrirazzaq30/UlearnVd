package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class CourseDiikuti {

    private String gambarCourse, namaCourse, kelasCourse, guruCourse;

    public CourseDiikuti(String gambarCourse, String namaCourse, String kelasCourse, String guruCourse) {
        this.gambarCourse = gambarCourse;
        this.namaCourse = namaCourse;
        this.kelasCourse = kelasCourse;
        this.guruCourse = guruCourse;
    }

    public CourseDiikuti(String namaCourse, String kelasCourse, String guruCourse) {
        this.namaCourse = namaCourse;
        this.kelasCourse = kelasCourse;
        this.guruCourse = guruCourse;
    }

    public String getGambarCourse() {
        return gambarCourse;
    }

    public void setGambarCourse(String gambarCourse) {
        this.gambarCourse = gambarCourse;
    }

    public String getNamaCourse() {
        return namaCourse;
    }

    public void setNamaCourse(String namaCourse) {
        this.namaCourse = namaCourse;
    }

    public String getKelasCourse() {
        return kelasCourse;
    }

    public void setKelasCourse(String kelasCourse) {
        this.kelasCourse = kelasCourse;
    }

    public String getGuruCourse() {
        return guruCourse;
    }

    public void setGuruCourse(String guruCourse) {
        this.guruCourse = guruCourse;
    }
}
