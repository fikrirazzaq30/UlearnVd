package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TELC2 on 8/12/2017.
 */

public class CourseHomeLessonBab {

    String gambarLesson, nomorLesson, namaLesson;

    public CourseHomeLessonBab(String gambarLesson, String nomorLesson, String namaLesson) {
        this.gambarLesson = gambarLesson;
        this.nomorLesson = nomorLesson;
        this.namaLesson = namaLesson;
    }

    public CourseHomeLessonBab(String nomorLesson, String namaLesson) {
        this.nomorLesson = nomorLesson;
        this.namaLesson = namaLesson;
    }

    public String getGambarLesson() {
        return gambarLesson;
    }

    public void setGambarLesson(String gambarLesson) {
        this.gambarLesson = gambarLesson;
    }

    public String getNomorLesson() {
        return nomorLesson;
    }

    public void setNomorLesson(String nomorLesson) {
        this.nomorLesson = nomorLesson;
    }

    public String getNamaLesson() {
        return namaLesson;
    }

    public void setNamaLesson(String namaLesson) {
        this.namaLesson = namaLesson;
    }
}
