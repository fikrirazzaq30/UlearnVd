package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TEL-C on 8/8/17.
 */

public class CourseKuis {

    private String nomorKuis, judulLessonBab, jmlPertanyaan, waktu, skor, lulus;

    public CourseKuis(String nomorKuis, String judulLessonBab, String jmlPertanyaan, String waktu, String skor, String lulus) {
        this.nomorKuis = nomorKuis;
        this.judulLessonBab = judulLessonBab;
        this.jmlPertanyaan = jmlPertanyaan;
        this.waktu = waktu;
        this.skor = skor;
        this.lulus = lulus;
    }

    public String getNomorKuis() {
        return nomorKuis;
    }

    public void setNomorKuis(String nomorKuis) {
        this.nomorKuis = nomorKuis;
    }

    public String getJudulLessonBab() {
        return judulLessonBab;
    }

    public void setJudulLessonBab(String judulLessonBab) {
        this.judulLessonBab = judulLessonBab;
    }

    public String getJmlPertanyaan() {
        return jmlPertanyaan;
    }

    public void setJmlPertanyaan(String jmlPertanyaan) {
        this.jmlPertanyaan = jmlPertanyaan;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getSkor() {
        return skor;
    }

    public void setSkor(String skor) {
        this.skor = skor;
    }

    public String getLulus() {
        return lulus;
    }

    public void setLulus(String lulus) {
        this.lulus = lulus;
    }
}