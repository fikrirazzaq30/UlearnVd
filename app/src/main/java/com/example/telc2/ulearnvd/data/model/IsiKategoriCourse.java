package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TEL-C on 8/9/17.
 */

public class IsiKategoriCourse {

    private String namaIsiKategoriCourse, gambarIsiKategoriCourse;

    public IsiKategoriCourse(String namaIsiKategoriCourse, String gambarIsiKategoriCourse) {
        this.namaIsiKategoriCourse = namaIsiKategoriCourse;
        this.gambarIsiKategoriCourse = gambarIsiKategoriCourse;
    }

    public IsiKategoriCourse(String namaIsiKategoriCourse) {
        this.namaIsiKategoriCourse = namaIsiKategoriCourse;
    }

    public String getNamaIsiKategoriCourse() {
        return namaIsiKategoriCourse;
    }

    public void setNamaIsiKategoriCourse(String namaIsiKategoriCourse) {
        this.namaIsiKategoriCourse = namaIsiKategoriCourse;
    }

    public String getGambarIsiKategoriCourse() {
        return gambarIsiKategoriCourse;
    }

    public void setGambarIsiKategoriCourse(String gambarIsiKategoriCourse) {
        this.gambarIsiKategoriCourse = gambarIsiKategoriCourse;
    }
}
