package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class KategoriCourse {

    private String namaKategori, gambarKategori;

    public KategoriCourse(String namaKategori) {
        this.namaKategori = namaKategori;
    }

    public KategoriCourse(String namaKategori, String gambarKategori) {
        this.namaKategori = namaKategori;
        this.gambarKategori = gambarKategori;
    }

    public String getNamaKategori() {
        return namaKategori;
    }

    public void setNamaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
    }

    public String getGambarKategori() {
        return gambarKategori;
    }

    public void setGambarKategori(String gambarKategori) {
        this.gambarKategori = gambarKategori;
    }
}