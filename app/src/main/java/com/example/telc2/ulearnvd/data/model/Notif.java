package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class Notif {

    private String namaProfil, namaCourse, gambar, waktu;

    public Notif(String namaProfil, String namaCourse, String gambar, String waktu) {
        this.namaProfil = namaProfil;
        this.namaCourse = namaCourse;
        this.gambar = gambar;
        this.waktu = waktu;
    }

    public Notif(String namaProfil, String namaCourse, String waktu) {
        this.namaProfil = namaProfil;
        this.namaCourse = namaCourse;
        this.waktu = waktu;
    }

    public String getNamaProfil() {
        return namaProfil;
    }

    public void setNamaProfil(String namaProfil) {
        this.namaProfil = namaProfil;
    }

    public String getNamaCourse() {
        return namaCourse;
    }

    public void setNamaCourse(String namaCourse) {
        this.namaCourse = namaCourse;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }
}
