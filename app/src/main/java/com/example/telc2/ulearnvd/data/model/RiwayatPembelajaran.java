package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class RiwayatPembelajaran {

    String nomerLesson, namaLesson, gambarCourse;

    public RiwayatPembelajaran(String nomerLesson, String namaLesson) {
        this.nomerLesson = nomerLesson;
        this.namaLesson = namaLesson;
    }

    public RiwayatPembelajaran(String nomerLesson, String namaLesson, String gambarCourse) {
        this.nomerLesson = nomerLesson;
        this.namaLesson = namaLesson;
        this.gambarCourse = gambarCourse;
    }

    public String getNomerLesson() {
        return nomerLesson;
    }

    public void setNomerLesson(String nomerLesson) {
        this.nomerLesson = nomerLesson;
    }

    public String getNamaLesson() {
        return namaLesson;
    }

    public void setNamaLesson(String namaLesson) {
        this.namaLesson = namaLesson;
    }

    public String getGambarCourse() {
        return gambarCourse;
    }

    public void setGambarCourse(String gambarCourse) {
        this.gambarCourse = gambarCourse;
    }
}
