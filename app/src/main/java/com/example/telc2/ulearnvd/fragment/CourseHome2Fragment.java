package com.example.telc2.ulearnvd.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.TabbedLesson2Activity;
import com.example.telc2.ulearnvd.activity.TabbedLessonActivity;
import com.example.telc2.ulearnvd.adapter.CourseHomeLessonBabAdapter;
import com.example.telc2.ulearnvd.data.model.CourseHomeLessonBab;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseHome2Fragment extends Fragment implements ItemClickListener {

    private List<CourseHomeLessonBab> courseHomeLessonBabList = new ArrayList<>();
    private CourseHomeLessonBabAdapter courseHomeLessonBabAdapter;
    private RecyclerView rcvCourseHomeLessonBab;

    public CourseHome2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_course_home2, container, false);

        //--Pair View
        rcvCourseHomeLessonBab = (RecyclerView) v.findViewById(R.id.rcv_detil_course_lessonbab);

        //--Adapter Riwayat Pembelajaran
        courseHomeLessonBabAdapter = new CourseHomeLessonBabAdapter(courseHomeLessonBabList);
        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(getContext());
        rcvCourseHomeLessonBab.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        rcvCourseHomeLessonBab.setLayoutManager(mLayoutManagerCourse);
        rcvCourseHomeLessonBab.setItemAnimator(new DefaultItemAnimator());
        rcvCourseHomeLessonBab.setAdapter(courseHomeLessonBabAdapter);
        courseHomeLessonBabAdapter.setClickListener(this);

        //--Populate Data LessonBab
        courseHomeLessonBabList.clear();
        dummyLessonBabData();

        return v;
    }

    public void dummyLessonBabData() {
        CourseHomeLessonBab lessonBab;

        lessonBab = new CourseHomeLessonBab("BAB 1", "Dasar Ekonomi");
        courseHomeLessonBabList.add(lessonBab);

        lessonBab = new CourseHomeLessonBab("BAB 2", "Hukum Permintaan dan Penawaran");
        courseHomeLessonBabList.add(lessonBab);

        courseHomeLessonBabAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view, int position) {
        final CourseHomeLessonBab courseHomeLessonBab = courseHomeLessonBabList.get(position);

        if (courseHomeLessonBab.getNomorLesson().equals("BAB 1")) {
            Intent intent = new Intent(getContext(), TabbedLesson2Activity.class);
            startActivity(intent);
        } else if (courseHomeLessonBab.getNomorLesson().equals("BAB 2")) {
        } else if (courseHomeLessonBab.getNomorLesson().equals("BAB 3")) {
        } else if (courseHomeLessonBab.getNomorLesson().equals("BAB 4")) {
        }
    }
}