package com.example.telc2.ulearnvd.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.DashboardActivity;
import com.example.telc2.ulearnvd.activity.IsiKategoriCourse2Activity;
import com.example.telc2.ulearnvd.activity.IsiKategoriCourseActivity;
import com.example.telc2.ulearnvd.activity.KategoriCourseActivity;
import com.example.telc2.ulearnvd.adapter.KategoriCourseAdapter;
import com.example.telc2.ulearnvd.data.model.KategoriCourse;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment implements ItemClickListener {

    private RelativeLayout rlKategori;

    private List<KategoriCourse> kategoriCourseList = new ArrayList<>();
    private KategoriCourseAdapter kategoriCourseAdapter;
    private RecyclerView rcvKategoriCourse;

    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_kategori_course, container, false);

        //--Pair View
        rlKategori = (RelativeLayout) v.findViewById(R.id.rl_explore_kategori_pembelajaran);


        //--Pair View
        rcvKategoriCourse = (RecyclerView) v.findViewById(R.id.rcv_kategori_course);

        //--Adapter Riwayat Pembelajaran
        kategoriCourseAdapter = new KategoriCourseAdapter(kategoriCourseList, getContext());
        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(getContext());
        rcvKategoriCourse.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        rcvKategoriCourse.setLayoutManager(mLayoutManagerCourse);
        rcvKategoriCourse.setItemAnimator(new DefaultItemAnimator());
        rcvKategoriCourse.setAdapter(kategoriCourseAdapter);
        kategoriCourseAdapter.setClickListener(this);


        //--Enable Click Handling
//        rlKategori.setOnClickListener(this);

        kategoriCourseList.clear();
        dummyKategoriCourseData();

        return v;
    }

    public void dummyKategoriCourseData() {
        KategoriCourse kategoriCourse;

        kategoriCourse = new KategoriCourse("Biologi", "http://juvetic.telclab.com/uelarn/images/Ipa%20besar.png");
        kategoriCourseList.add(kategoriCourse);

        kategoriCourse = new KategoriCourse("Ekonomi", "http://juvetic.telclab.com/uelarn/images/IPS%20besar.png");
        kategoriCourseList.add(kategoriCourse);
        
        kategoriCourseAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view, int position) {
        final KategoriCourse kategoriCourse = kategoriCourseList.get(position);
        if (position == 0) {
            Intent intent = new Intent(getContext(), IsiKategoriCourseActivity.class);
            startActivity(intent);
        } else if (position == 1) {
            Intent intent = new Intent(getContext(), IsiKategoriCourse2Activity.class);
            startActivity(intent);
        }

    }
}