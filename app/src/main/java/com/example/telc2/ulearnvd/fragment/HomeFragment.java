package com.example.telc2.ulearnvd.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.CompleteCourseActivity;
import com.example.telc2.ulearnvd.activity.DashboardActivity;
import com.example.telc2.ulearnvd.activity.HasilKuisActivity;
import com.example.telc2.ulearnvd.activity.LoginActivity;
import com.example.telc2.ulearnvd.activity.RegistrasiActivity;
import com.example.telc2.ulearnvd.activity.TabbedLessonActivity;
import com.example.telc2.ulearnvd.adapter.RiwayatPembelajaranAdapter;
import com.example.telc2.ulearnvd.adapter.RiwayatUjianAdapter;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.data.model.Notif;
import com.example.telc2.ulearnvd.data.model.RiwayatPembelajaran;
import com.example.telc2.ulearnvd.data.model.RiwayatUjian;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, ItemClickListener{

    private Button btnExplore;
    private RelativeLayout rlProfil, rlRiwayatPembelajaran, rlRiwayatUjian, rlRiwayatKosong;
    private TextView txRiwayatPembelajaran, txRiwayatUjian, txNamaLengkap;
    private List<RiwayatPembelajaran> riwayatPembelajaranList = new ArrayList<>();
    private List<RiwayatUjian> riwayatUjianList = new ArrayList<>();
    private RiwayatPembelajaranAdapter pembelajaranAdapter;
    private RiwayatUjianAdapter ujianAdapter;
    private RecyclerView rcvPembelajaran, rcvUjian;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        //--Pair View
        btnExplore = (Button) v.findViewById(R.id.btn_home_explore);
        rlProfil = (RelativeLayout) v.findViewById(R.id.rl_home_profil);
        rlRiwayatPembelajaran = (RelativeLayout) v.findViewById(R.id.rl_home_riwayat_pembelajaran);
        rlRiwayatUjian = (RelativeLayout) v.findViewById(R.id.rl_home_riwayat_ujian);
        rlRiwayatKosong = (RelativeLayout) v.findViewById(R.id.rl_home_riwayat_kosong);
        txRiwayatPembelajaran = (TextView) v.findViewById(R.id.tx_home_riwayat_pembelajaran);
        txRiwayatUjian = (TextView) v.findViewById(R.id.tx_home_riwayat_ujian);
//        rcvPembelajaran = (RecyclerView) v.findViewById(R.id.rcv_home_riwayat_pembelajaran);
//        rcvUjian = (RecyclerView) v.findViewById(R.id.rcv_home_riwayat_ujian);
        txNamaLengkap = (TextView) v.findViewById(R.id.tx_home_nama);

        Intent intent = getActivity().getIntent();
        if (savedInstanceState == null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if(extras == null) {
                txNamaLengkap.setText("");
            } else {
                txNamaLengkap.setText(extras.getString("namalengkap"));
            }
        } else {
            txNamaLengkap.setText(savedInstanceState.getSerializable("namalengkap").toString());
        }


        //--Enable Click Handling
        btnExplore.setOnClickListener(this);
        rlRiwayatPembelajaran.setOnClickListener(this);
        rlRiwayatUjian.setOnClickListener(this);

        //--Adapter Riwayat Pembelajaran
//        pembelajaranAdapter = new RiwayatPembelajaranAdapter(riwayatPembelajaranList, getContext());
//        RecyclerView.LayoutManager mLayoutManagerPembelajaran = new LinearLayoutManager(getContext());
//        rcvPembelajaran.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
//        rcvPembelajaran.setLayoutManager(mLayoutManagerPembelajaran);
//        rcvPembelajaran.setItemAnimator(new DefaultItemAnimator());
//        rcvPembelajaran.setAdapter(pembelajaranAdapter);
//        pembelajaranAdapter.setClickListener(this);

        //--Populate Data Riwayat Pembelajaran
        riwayatPembelajaranList.clear();
//        dummyPembelajaranData();


        //--Adapter Riwayat Ujian
//        ujianAdapter = new RiwayatUjianAdapter(riwayatUjianList, getContext());
//        RecyclerView.LayoutManager mLayoutManagerUjian = new LinearLayoutManager(getContext());
//        rcvUjian.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
//        rcvUjian.setLayoutManager(mLayoutManagerUjian);
//        rcvUjian.setItemAnimator(new DefaultItemAnimator());
//        rcvUjian.setAdapter(ujianAdapter);
//        ujianAdapter.setClickListener(this);

        //--Populate Data Riwayat Ujian
        riwayatUjianList.clear();
//        dummyUjianData();

        return v;
    }

    @Override
    public void onClick(View v) {
        if (v == btnExplore) {
            rlRiwayatKosong.setVisibility(View.GONE);
            rlRiwayatPembelajaran.setVisibility(View.VISIBLE);
            rlRiwayatUjian.setVisibility(View.VISIBLE);
            txRiwayatUjian.setVisibility(View.VISIBLE);
            txRiwayatPembelajaran.setVisibility(View.VISIBLE);
            final ViewPager vp = (ViewPager) getActivity().findViewById(R.id.viewpager_dashboard);
            vp.setCurrentItem(1);
        } else if (v == rlRiwayatPembelajaran) {
            Intent intent = new Intent(getContext(), TabbedLessonActivity.class);
            startActivity(intent);
        } else if (v == rlRiwayatUjian) {
            Intent intent = new Intent(getContext(), HasilKuisActivity.class);
            startActivity(intent);
        }
    }

    private void dummyPembelajaranData() {
        RiwayatPembelajaran pembelajaran;

        pembelajaran = new RiwayatPembelajaran("Pelajaran", "Biologi Kelas 12");
        riwayatPembelajaranList.add(pembelajaran);

        pembelajaranAdapter.notifyDataSetChanged();
    }

    private void dummyUjianData() {
        RiwayatUjian ujian;

        ujian = new RiwayatUjian("Kuis 1 ", "Biologi Kelas 12", "3/5");
        riwayatUjianList.add(ujian);

        ujianAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view, int position) {
        Intent intent = new Intent(getContext(), CompleteCourseActivity.class);
        startActivity(intent);
    }
}
