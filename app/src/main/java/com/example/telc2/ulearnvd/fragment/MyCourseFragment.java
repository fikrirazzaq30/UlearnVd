package com.example.telc2.ulearnvd.fragment;


import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.CompleteCourse2Activity;
import com.example.telc2.ulearnvd.activity.CompleteCourseActivity;
import com.example.telc2.ulearnvd.activity.DashboardActivity;
import com.example.telc2.ulearnvd.adapter.CourseDiikutiAdapter;
import com.example.telc2.ulearnvd.adapter.RiwayatPembelajaranAdapter;
import com.example.telc2.ulearnvd.adapter.RiwayatUjianAdapter;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.data.model.RiwayatPembelajaran;
import com.example.telc2.ulearnvd.data.model.RiwayatUjian;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCourseFragment extends Fragment implements ItemClickListener {

    private List<CourseDiikuti> courseDiikutiList = new ArrayList<>();
    private CourseDiikutiAdapter courseDiikutiAdapter;
    private RecyclerView rcvCourseDiikuti;
    private RelativeLayout rlMyCourseDownload;
    private Switch swDownload;

    public MyCourseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_course, container, false);

        //--Pair View
        rcvCourseDiikuti = (RecyclerView) v.findViewById(R.id.rcv_mycourse_course_diikuti);
        rlMyCourseDownload = (RelativeLayout) v.findViewById(R.id.rl_mycourse_download);
        swDownload = (Switch) v.findViewById(R.id.sw_mycourse_download_otomatis);

        //--Adapter Riwayat Pembelajaran
        courseDiikutiAdapter = new CourseDiikutiAdapter(courseDiikutiList, getContext());
        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(getContext());
        rcvCourseDiikuti.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        rcvCourseDiikuti.setLayoutManager(mLayoutManagerCourse);
        rcvCourseDiikuti.setItemAnimator(new DefaultItemAnimator());
        rcvCourseDiikuti.setAdapter(courseDiikutiAdapter);
        courseDiikutiAdapter.setClickListener(this);

        rlMyCourseDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swDownload.toggle();
                NotificationManager notif=(NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                Notification notify = new Notification.Builder
                        (getContext()).setContentTitle("Notifikasi").setContentText("Download Konten Otomatis Aktif").
                        setContentTitle("Ulearning").setSmallIcon(R.mipmap.ic_launcher).build();

                notify.flags |= Notification.FLAG_AUTO_CANCEL;
                notif.notify(0, notify);
            }
        });

        //--Populate Data Course Diikuti
        courseDiikutiList.clear();
        dummyCourseData();

        return v;
    }

    public void dummyCourseData() {
        CourseDiikuti courseDiikuti;

        courseDiikuti = new CourseDiikuti("http://juvetic.telclab.com/uelarn/images/Ipa%20besar.png", "Biologi ", "Kelas 12", "Maman, S.Pd.");
        courseDiikutiList.add(courseDiikuti);

        courseDiikuti = new CourseDiikuti("http://juvetic.telclab.com/uelarn/images/IPS%20besar.png", "Ekonomi ", "Kelas 12", "Mimin, S.Pd.");
        courseDiikutiList.add(courseDiikuti);
//
//        courseDiikuti = new CourseDiikuti("http://juvetic.telclab.com/uelarn/images/Matematika%20besar.png", "Matematika ", "Kelas 12", "Mumun, S.Pd.");
//        courseDiikutiList.add(courseDiikuti);


        courseDiikutiAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view, int position) {
        final CourseDiikuti courseDiikuti = courseDiikutiList.get(position);
        if (position == 0) {
            Intent intent = new Intent(getContext(), CompleteCourseActivity.class);
            startActivity(intent);
        } else if (position == 1) {
            Intent intent = new Intent(getContext(), CompleteCourse2Activity.class);
            startActivity(intent);
        }

    }
}
