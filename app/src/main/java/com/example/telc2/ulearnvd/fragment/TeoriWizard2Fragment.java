package com.example.telc2.ulearnvd.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.telc2.ulearnvd.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeoriWizard2Fragment extends Fragment {

    private TextView txJudul, txIsi1, txIsi2, txIsi3, txIsi4;
    private VideoView videoLesson;
    private LinearLayout linearVideo, linearTeori, linearAssignment;
    private Button btnTeori, btnVideo, btnAssignment;
    private Button btnSebelumnya, btnSelanjutnya;
    private TextView halaman;
    ProgressDialog pDialog;
    String VideoURL = "http://juvetic.telclab.com/pertumbuhan%20dan%20perkembangan%20tumbuhan.mp4";


    public TeoriWizard2Fragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_teori_wizard2, container, false);

        String isi1 = "Ekonomi adalah pengetahuan sosial, berkaitan dengan perilaku manusia\n" +
                "dan sistem sosial, dimana manusia mengorganisasikan aktivitas-aktivitasnya\n" +
                "dalam rangka pemuasan kebutuhan dasar (makan atau pangan, pakaian atau\n" +
                "sandang, dan tempat tinggal atau papan), serta pemenuhan kebutuhan nonmateri\n" +
                "(pendidikan, rekreasi, keindahan, spiritual dan sebagainya).";
        String isi2 = "Berbagai macam aktivitas dan perilaku manusia untuk memenuhi\n" +
                "kebutuhan hidupnya itulah yang disebut dengan kegiatan ekonomi. Perilaku dan\n" +
                "aktivitas manusia dalam memenuhi segala kebutuhan hidup tersebut tidaklah\n" +
                "sama, tergantung pada situasi, kondisi, waktu, dan lokasi. Karakter kegiatan\n" +
                "ekonomi manusia yang ada di permukaan bumi hanya bersifat kecenderungan,\n" +
                "jadi tidak bersifat permanen. Inilah yang menjadi dasar dalam pengkajian ilmu\n" +
                "ekonomi.";
        String isi3 = "Ekonomi secara umum merupakan studi dan latihan memilih (the study\n" +
                "and exercise of choice). Didalamnya meliputi tingkah laku manusia dalam\n" +
                "memilih barang dan jasa yang diperlukan untuk memenuhi kebutuhan hidupnya.\n" +
                "Berbagai macam kebutuhan manusia diwujudkan dalam bentuk benda\n" +
                "materi (pangan, sandang, papan, dan sebagainya) serta jasa-jasa (perawatan\n" +
                "kesehatan, pendidikan, keamanan, rekreasi, dan sebagainya) yang jumlahnya\n" +
                "terbatas. Keterbatasan inilah yang menyebabkan manusia harus memilih secara\n" +
                "cerdas dan terampil.";
        String isi4 = "Pada dasarnya semua kegiatan ekonomi mengandung prinsip efisiensi\n" +
                "atau ekonomis, artinya bagaimana memperoleh satu (unit) barang atau jasa\n" +
                "yang akan dipergunakan untuk memenuhi kebutuhan hidupnya tersebut dengan\n" +
                "menggunakan atau mengeluarkan biaya paling rendah.\n" +
                "Cakupan ilmu ekonomi sangat luas meliputi bagaimana upaya masyarakat, baik nasional maupun intemasional memenuhi kebutuhan dan mencapai kemakmuran. Dengan demikian, tidaklah mudah merumuskan definisi ilmu ekonomi yang tepat dan singkat.";

        txJudul = (TextView) v.findViewById(R.id.tx_teori_wizard_lesson_judul);
        txIsi1 = (TextView) v.findViewById(R.id.tx_teori_wizard_lesson_isi1);
        txIsi2 = (TextView) v.findViewById(R.id.tx_teori_wizard_lesson_isi2);
        txIsi3 = (TextView) v.findViewById(R.id.tx_teori_wizard_lesson_isi3);
        txIsi4 = (TextView) v.findViewById(R.id.tx_teori_wizard_lesson_isi4);


        btnSebelumnya = (Button) v.findViewById(R.id.btn_teori_sebelumnya);
        btnSebelumnya.setVisibility(View.INVISIBLE);
        btnSelanjutnya = (Button) v.findViewById(R.id.btn_teori_selanjutnya);

        halaman = (TextView) v.findViewById(R.id.tx_teori_halaman);

        txIsi1.setText(Html.fromHtml(isi1));
        txIsi2.setText(Html.fromHtml(isi2));
        txIsi3.setText(Html.fromHtml(isi3));
        txIsi4.setText(Html.fromHtml(isi4));

        final ViewPager vp = (ViewPager) getActivity().findViewById(R.id.container2);

        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() + 1);
            }
        });


        return v;
    }

}
