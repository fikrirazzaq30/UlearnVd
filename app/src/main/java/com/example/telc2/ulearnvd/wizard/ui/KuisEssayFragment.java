package com.example.telc2.ulearnvd.wizard.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KuisEssayFragment extends Fragment {


    public KuisEssayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_kuis_essay, container, false);

        return rootView;
    }

}